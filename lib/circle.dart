import 'dart:math';

import 'package:lecture_solid/figure.dart';

class Circle implements Figure{
  double radius;

  Circle(this.radius);

  @override
  void area() {
    var area = radius*2*pi;
    print(area.toString());
  }
}