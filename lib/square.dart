import 'package:lecture_solid/figure.dart';

class Square implements Figure{
  double length;

  Square(this.length);

  @override
  void area() {
    var area = length*length;
    print(area.toString());
  }
}